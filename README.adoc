= Antora Blog Pipeline Extension and UI extension
:uri-antora: https://antora.org/
:uri-yuzutech: https://github.com/yuzutech/blog.yuzutech.fr

This project is an adaptation of the {uri-yuzutech}[blog.yuzutech.fr] project.

It relies on two experimental/POC projects, pipeline extensions and ui builder.

There are two parts, under packages:

* antora-blog, an Antora pipeline extension incorporating the blog generation code
* blog-ui, a UI extension incorporating the blog UI extensions.

See the documentation under the packages.
