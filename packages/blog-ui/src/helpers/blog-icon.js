'use strict'

const icon = require('@fortawesome/fontawesome-svg-core').icon
const icons = require('@fortawesome/free-solid-svg-icons')

module.exports = (iconName, { data, hash: context }) => {
  const { page } = data.root
  return icon(icons[iconName], { styles: { color: page.attributes['color-secondary'] } }).html
}
