'use strict'

const createRSSPage = require('./rss').createRSSPage
const { createTagPage } = require('./tagPage')
const { createIndexPage, indexContents } = require('./indexPage')

module.exports.register = (eventEmitter, config = {}) => {
  const component = config.component || 'blog'
  const version = config.version || 'master'
  const module = config.module || 'ROOT'
  const topic = config.topic || ''
  const blogTitle = config.title || 'Blog'
  const description = config.description || 'description'

  const model = {
    blogTitle,
    description,
    component,
    version,
    module,
    topic,
  }

  /* Create an empty, dummy, index page that can be identified by the "start_page" mechanism.
   See https://gitlab.com/antora/antora/-/issues/708
   */
  eventEmitter.on('beforeClassifyContent',
    ({ context, contentAggregate }) => {
      const playbook = ('playbook' in context) ? context.playbook : context
      model.url = playbook.site.url
      contentAggregate.forEach((cv) => {
        if ((cv.name === component) && (cv.version === version)) {
          cv.files.push(createIndexPage(model))
        }
      })
    }
  )

  eventEmitter.on('onDocumentHeadersParsed', ({ pagesWithHeaders, contentCatalog }) => {
    function addFile (file) {
      pagesWithHeaders.push(contentCatalog.addFile(file))
    }

    const indexPage = contentCatalog.findBy({ component, version, module, family: 'page', relative: 'index.adoc' })[0]
    const articles = contentCatalog.findBy({ component, version, module, family: 'page' })
      .filter((file) => file.asciidoc && file !== indexPage)
    contentCatalog.addFile(createRSSPage(model, articles))
    const tags = getTags(articles)
    for (const [tag, articlesWithTag] of tags) {
      addFile(createTagPage(tag, model, articlesWithTag))
    }
    indexContents(indexPage, Array.from(tags.keys()), model, articles)
  })
}

function getTags (articles) {
  const result = new Map()
  articles.forEach((file) => {
    const pageTags = file.asciidoc.attributes['page-tags']
    if (pageTags) {
      pageTags.split(',').map((tag) => tag.trim()).forEach((tag) => {
        const files = result.get(tag)
        files ? files.push(file) : result.set(tag, [file])
      })
    }
  })
  return result
}
