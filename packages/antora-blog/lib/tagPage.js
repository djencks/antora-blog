'use strict'

const { cardHtml } = require('./_card')
const { getPath } = require('./_tag')

function convert (uiModel, tag, tagPages, relPath) {
  const pagesHTML = cardHtml(uiModel, tagPages, relPath)
  const content = `++++
  <section class="section">
    <div class="container">
      <div class="columns is-multiline">
        ${pagesHTML.join('\n')}
      </div>
    </div>
  </section>
++++
`
  return content
}

function createTagPage (tag, uiModel, files) {
  const tagPathSegment = tag.toLowerCase().replace('.', '-')
  // const tagUrl = getTagUrl(tag)
  const contents = convert(uiModel, tag, files, '../../')
  return {
    title: `Blog / ${tag} - ${uiModel.blogTitle}`,
    path: getPath('modules', uiModel.module, 'pages', uiModel.topic, 'tag', tagPathSegment, 'index.adoc'),
    contents: Buffer.from(contents),
    asciidoc: {
      attributes: {
        'page-layout': 'blog-index',
        'page-tag': tag,
        'page-blog-tags-array': `<span class="tag active is-medium"> ${tag} <a href="../../index.html" class="delete is-small"></a></span>`,
        doctitle: `${tag} - ${uiModel.blogTitle}`,
      },
      navtitle: tag,
      xreftext: tag,
    },
    src: {
      module: uiModel.module,
      component: uiModel.component,
      version: uiModel.version,
      relative: getPath('tag', tagPathSegment, 'index.adoc'),
      family: 'page',
    },
  }
}

module.exports = { createTagPage }
