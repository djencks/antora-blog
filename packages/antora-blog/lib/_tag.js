'use strict'

function getTagUrl (uiModel, tag, relPath) {
  return `${relPath}tag/${tag.toLowerCase().replace('.', '-')}/index.html`
}

function tagCloud (uiModel, tags, relPath) {
  return tags.map((tag) => tag.trim()).map((tag) => `<a href="${getTagUrl(uiModel, tag, relPath)}" class="tag is-medium">${tag}</a>`)
}

function getTagUrlAdoc (tag) {
  return `tag/${tag.toLowerCase().replace('.', '-')}/index.adoc`
}

function tagCloudAdoc (tags) {
  return tags.map((tag) => tag.trim()).map((tag) => `xref:${getTagUrlAdoc(tag)}[${tag},role="tag is-medium"]`)
}

function getPath (...segments) {
  return segments.filter((segment) => segment).join('/')
}

module.exports = {
  getPath,
  tagCloud,
  getTagUrl,
  tagCloudAdoc,
  getTagUrlAdoc,
}
