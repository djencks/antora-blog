'use strict'

const blogVersion = require('../package.json').version

function categories (article) {
  if (article.asciidoc.attributes['page-tags']) {
    return article.asciidoc.attributes['page-tags']
      .split(',')
      .map((tag) => tag.trim())
  }
  return []
}

function atomItemModel (uiModel, article) {
  const attributes = article.asciidoc.attributes
  const revisionDate = new Date(attributes['page-revdate'])
  const image = attributes['page-image']
  return {
    title: article.asciidoc.doctitle,
    description: attributes.description,
    creator: attributes['page-author'],
    categories: categories(article),
    pubDate: revisionDate.toUTCString(),
    image: image,
    contents: article.contents,
    path: article.out.path,
  }
}

function generateAtomFeedModel (uiModel, articles) {
  const buildDate = new Date().toUTCString()
  const atomItems = articles.map((article) => atomItemModel(uiModel, article))
  return {
    title: uiModel.title,
    url: uiModel.url,
    description: 'uiModel.description',
    favicon: `${uiModel.url}/favicon.ico`,
    link: `${uiModel.url}/rss/feed.xml`,
    ttl: 60,
    lastBuildDate: buildDate,
    generator: `Antora Blog Extension ${blogVersion}`,
    items: atomItems,
  }
}

function atomItem (uiModel, item) {
  const categories = item.categories.map((category) => `<category><![CDATA[${category}]]></category>`)
  return `<item>
    <title><![CDATA[${item.title}]]></title>
    <description><![CDATA[${item.description}]]></description>
    <link>${item.canonicalUrl}</link>
    ${categories.join('\n')}
    <dc:creator><![CDATA[${item.creator}]]></dc:creator>
    <pubDate>${item.pubDate}</pubDate>
    <media:content url="${uiModel.url}/_/images/${item.image}" medium="image"/>
    <content:encoded><![CDATA[${item.contents}]]></content:encoded>
  </item>`
  // <media:content url="${uiModel.site.url}${uiModel.site.ui.url}/images/${item.image}" medium="image"/>
}

function generateAtomFeed (uiModel, atomFeedModel) {
  const atomItems = atomFeedModel.items.map((item) => atomItem(uiModel, item))
  return `<?xml version="1.0" encoding="UTF-8"?>
  <rss xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:atom="http://www.w3.org/2005/Atom" version="2.0" xmlns:media="http://search.yahoo.com/mrss/">
    <channel>
      <title><![CDATA[${atomFeedModel.title}]]></title>
      <description><![CDATA[${atomFeedModel.description}]]></description>
      <link>${atomFeedModel.url}</link>
      <image>
        <url>${atomFeedModel.favicon}</url>
        <title>${atomFeedModel.title}</title>
        <link>${atomFeedModel.url}</link>
      </image>
      <generator>${atomFeedModel.generator}</generator>
      <lastBuildDate>${atomFeedModel.lastBuildDate}</lastBuildDate>
      <atom:link href="${atomFeedModel.link}" rel="self" type="application/rss+xml"/>
      <ttl>${atomFeedModel.ttl}</ttl>
      ${atomItems.join('\n')}
    </channel>
  </rss>`
}

function createRSSPage (uiModel, articles) {
  const atomFeedModel = generateAtomFeedModel(uiModel, articles)
  atomFeedModel.items = atomFeedModel.items.map((item) => {
    // noinspection JSPrimitiveTypeWrapperUsage, JSUndefinedPropertyAssignment
    item.canonicalUrl = articles.filter((article) => article.out.path === item.path)[0].pub.canonicalUrl
    return item
  })
  return {
    title: `Blog / RSS - ${uiModel.blogTitle}`,
    version: uiModel.version,
    mediaType: 'application/rss+xml',
    contents: Buffer.from(generateAtomFeed(uiModel, atomFeedModel)),
    asciidoc: {
      attributes: {
        // 'page-layout': 'feed',
      },
    },
    src: {
      module: uiModel.module,
      component: uiModel.component,
      version: uiModel.version,
      family: 'page',
      relative: 'rss/feed.xml',
    },
  }
}

module.exports.createRSSPage = createRSSPage
