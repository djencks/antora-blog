'use strict'

const { getPath, tagCloud } = require('./_tag')
const { cardHtml: card } = require('./_card')

function convert (tags, uiModel, pages) {
  const pagesHTML = card(uiModel, pages, '')
  const contents = `++++
  <section class="section">
    <div class="container">
      <div class="columns is-multiline">
        ${pagesHTML.join('\n')}
      </div>
    </div>
  </section>
++++`
  return contents
}

function indexContents (indexPage, tags, uiModel, files) {
  const tagsHTML = tagCloud(uiModel, tags, '')
  indexPage.contents = Buffer.from(convert(tags, uiModel, files))
  indexPage.asciidoc = {
    attributes: {
      'page-layout': 'blog-index',
      'page-tags': tags,
      'page-blog-tags-array': tagsHTML.join('\n'),
    },
    doctitle: uiModel.blogTitle,
    navtitle: uiModel.blogTitle,
    xreftext: uiModel.blogTitle,
  }
}

function createIndexPage ({ module, topic, blogTitle }) {
  const path = getPath('modules', module, 'pages', topic, 'index.adoc')
  return {
    contents: Buffer.from(''),
    path,
    src: {
      path,
      basename: 'index.adoc',
      stem: 'index',
      extname: '.adoc',
      origin: 'blog-plugin',
    },
  }
}

module.exports = { createIndexPage, indexContents }
