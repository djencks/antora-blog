const icon = require('@fortawesome/fontawesome-svg-core').icon
const faTag = require('@fortawesome/free-solid-svg-icons').faTag

const { getTagUrlAdoc, getTagUrl } = require('./_tag')
const { getShortRevisionDate } = require('./_date')

function getTags (page) {
  if (page.asciidoc && page.asciidoc.attributes && page.asciidoc.attributes['page-tags']) {
    return page.asciidoc.attributes['page-tags'].split(',').map((tag) => tag.trim())
  }
  return []
}

function cardAdoc (uiModel, tagPages, relPath) {
  return tagPages
    .sort((a1, a2) => new Date(a2.asciidoc.attributes['page-revdate']) - new Date(a1.asciidoc.attributes['page-revdate']))
    .map((page, index) => {
      const tagsHTML = getTags(page).map((tag) => `icon:${icon(faTag).html}[alt="${tag}", role="tag is-light has-icon">, link=${getTagUrlAdoc(tag)}]`)
      const attributes = page.asciidoc.attributes
      const image = attributes['page-image']
      const title = attributes.doctitle
      const description = attributes.description
      const revisionDate = new Date(attributes['page-revdate'])
      //TODO basename is not correct if there's a topic.
      const pageUrl = `${relPath}${page.out.basename}`
      const imageHTML = image ? `
[role="card-image"]
--
image::${image}[alt="${title} illustration", link="${page.src.relative}" role="summary"]
--
` : ''
      //TODO creating an anchor with block content probably requires a converter template, thus Asciidoctor 2.
      return `<div class="column${index === 0 ? ' is-full' : ' is-one-third'}">
  <div class="card article${index === 0 ? ' card-featured' : ''}">
    ${index < 7 ? imageHTML : ''}
    <div class="card-content">
      <a href="${pageUrl}" class="summary">
        <time datetime="${revisionDate}">${getShortRevisionDate(revisionDate)}</time>
        <h2 class="article-title">${title}</h2>
        <p class="excerpt">${description}</p>
      </a>
      <p class="tags is-condensed">
        ${tagsHTML.join('\n')}
      </p>
    </div>
  </div>
</div>`
    })
}

function cardHtml (uiModel, tagPages, relPath) {
  return tagPages
    .sort((a1, a2) => new Date(a2.asciidoc.attributes['page-revdate']) - new Date(a1.asciidoc.attributes['page-revdate']))
    .map((page, index) => {
      const tagsHTML = getTags(page).map((tag) => `<a href="${getTagUrl(uiModel, tag, relPath)}" class="tag is-light has-icon">
  <span class="icon">
    ${icon(faTag).html} ${tag}
  </span>
</a>`)
      const attributes = page.asciidoc.attributes
      const image = attributes['page-image']
      const title = attributes.doctitle
      const description = attributes.description
      const revisionDate = new Date(attributes['page-revdate'])
      //TODO basename is not correct if there's a topic.
      const pageUrl = `${relPath}${page.out.basename}`
      const imageHTML = image ? `<div class="card-image">
  <a href="${pageUrl}" class="summary">
    <img src="${relPath}_images/${image}" alt="${title} illustration">
  </a>
</div>` : ''
      return `<div class="column${index === 0 ? ' is-full' : ' is-one-third'}">
  <div class="card article${index === 0 ? ' card-featured' : ''}">
    ${index < 7 ? imageHTML : ''}
    <div class="card-content">
      <a href="${pageUrl}" class="summary">
        <time datetime="${revisionDate}">${getShortRevisionDate(revisionDate)}</time>
        <h2 class="article-title">${title}</h2>
        <p class="excerpt">${description}</p>
      </a>
      <p class="tags is-condensed">
        ${tagsHTML.join('\n')}
      </p>
    </div>
  </div>
</div>`
    })
}

module.exports = { cardHtml, cardAdoc }
